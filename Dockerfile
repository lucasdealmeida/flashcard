FROM php:8.1.3-apache
RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libicu-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libzip-dev \
    git \
    zip \
    g++ \
    wget \
    xvfb \
    unzip \
    gnupg

RUN docker-php-ext-configure intl
RUN docker-php-ext-configure gd --enable-gd --with-jpeg
RUN docker-php-ext-install intl pdo_mysql zip gd pcntl exif

RUN sed -i 's/var\/www\/html/var\/www\/html\/public/g' /etc/apache2/sites-enabled/000-default.conf
RUN a2enmod rewrite
