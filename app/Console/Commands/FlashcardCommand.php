<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FlashcardCommand extends Command
{
    protected $signature = 'flashcard:interactive';

    protected $description = 'Flashcard practice';

    public function handle()
    {
        $this->output->write(sprintf("\033\143"));

        $action = $this->choice(
            'What would you like to do?',
            [
                'Create a flashcard',
                'List all flashcards',
                'Practice',
                'Stats',
                'Reset',
                'Exit'
            ],
        );

        match ($action) {
            'Create a flashcard' => $this->call('flashcard:create'),
            'List all flashcards' => $this->call('flashcard:list'),
            'Practice' => $this->call('flashcard:practice'),
            'Stats' => $this->call('flashcard:stats'),
            'Reset' => $this->call('flashcard:reset'),
            default => '',
        };

        if ($action !== 'Exit') {
            $this->handle();
        }

        return 0;
    }
}
