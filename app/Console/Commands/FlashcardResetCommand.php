<?php

namespace App\Console\Commands;

use App\Models\Answer;
use Illuminate\Console\Command;

class FlashcardResetCommand extends Command
{
    protected $signature = 'flashcard:reset';

    protected $description = 'Delete all flashcards answers';

    public function handle()
    {
        $this->output->write(sprintf("\033\143"));

        if (!$this->confirm('Do you wish to continue?', true)) {
            return 0;
        }

        Answer::query()->delete();

        $this->output->write(sprintf("\033\143"));

        $this->info("The answers has been removed.");

        $this->anticipate('Press return to see the main menu', ['Enter'], 'Enter');

        return 0;
    }
}
