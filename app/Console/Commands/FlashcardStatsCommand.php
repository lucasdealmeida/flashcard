<?php

namespace App\Console\Commands;

use App\Models\Flashcard;
use App\Support\FlashcardCollection;
use Illuminate\Console\Command;

class FlashcardStatsCommand extends Command
{
    protected $signature = 'flashcard:stats';

    protected $description = 'Display flashcards statistics';

    public function handle()
    {
        /** @var FlashcardCollection $flashcards */
        $flashcards = Flashcard::query()->with('answer')->get();

        $percentageAnswered = $flashcards->onlyAnswered()->percentageOf($flashcards->count());
        $percentageCorrect = $flashcards->onlyCorrect()->percentageOf($flashcards->count());
        
        $this->output->write(sprintf("\033\143"));
        $this->info("{$flashcards->count()} questions in total.");
        $this->info("{$percentageAnswered}% of questions that have an answer.");
        $this->info("{$percentageCorrect}% of questions that have a correct answer.");

        $this->anticipate('Press return to see the main menu', ['Enter'], 'Enter');

        return 0;
    }
}
