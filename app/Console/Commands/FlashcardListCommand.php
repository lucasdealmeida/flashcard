<?php

namespace App\Console\Commands;

use App\Models\Flashcard;
use Illuminate\Console\Command;

class FlashcardListCommand extends Command
{
    protected $signature = 'flashcard:list';

    protected $description = 'List all flashcards';

    public function handle()
    {
        $this->output->write(sprintf("\033\143"));

        $rows = Flashcard::query()->get(['question', 'right_answer'])->toArray();

        $this->table(['Question', 'Answer'], $rows);

        $this->anticipate('Press return to see the main menu', ['Enter'], 'Enter');

        return 0;
    }
}
