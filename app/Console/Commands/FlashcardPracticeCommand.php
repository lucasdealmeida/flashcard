<?php

namespace App\Console\Commands;

use App\Models\Flashcard;
use App\Support\FlashcardCollection;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\Table;

class FlashcardPracticeCommand extends Command
{
    protected $signature = 'flashcard:practice';

    protected $description = 'Practice flashcards';

    protected FlashcardCollection $flashcards;

    protected Flashcard $flashcard;

    protected string $action;

    public function handle(bool $clearScreen = true)
    {
        /** @var FlashcardCollection $flashcards */
        $this->flashcards = Flashcard::query()->with('answer')->get();

        $this->displayQuestionsTable($clearScreen);
        $this->displayQuestionsChoice();

        if ($this->action === 'Back to main menu') {
            return 0;
        }

        $this->displayAskAnswer();
        $this->displayWhetherAnswerIsRight();
        $this->handle(false);
    }

    protected function displayQuestionsTable(bool $clearScreen): void
    {
        if ($clearScreen) {
            $this->output->write(sprintf("\033\143"));
        }

        $rows = $this->flashcards
            ->map
            ->only('question', 'status')
            ->toArray();

        $completionPercentage = $this->flashcards
            ->onlyCorrect()
            ->percentageOf($this->flashcards->count());

        (new Table($this->output))
            ->setHeaders(['Question', 'Status'])
            ->setRows($rows)
            ->setFooterTitle("{$completionPercentage}% of completion")
            ->render();
    }

    protected function displayQuestionsChoice(): void
    {
        $options = $this->flashcards
            ->exceptCorrect()
            ->pluck('question')
            ->push('Back to main menu');

        $this->action = $this->choice('Which question do you like to practice?', $options->toArray());
    }

    protected function displayAskAnswer(): void
    {
        $this->flashcard = Flashcard::query()
            ->where('question', $this->action)
            ->first();

        $answer = $this->ask("Write the answer for \"{$this->flashcard->question}\"");

        $this->flashcard
            ->answer()
            ->updateOrCreate([], ['answer' => $answer]);
    }

    protected function displayWhetherAnswerIsRight(): void
    {
        $this->output->write(sprintf("\033\143"));

        $message = '<fg=red>Answer is incorrect.</>';
        if ($this->flashcard->answer_is_right) {
            $message = '<fg=green>Answer is correct.</>';
        }

        $this->newLine();
        $this->line($message);
        $this->newLine();
    }
}
