<?php

namespace App\Console\Commands;

use App\Models\Flashcard;
use Illuminate\Console\Command;

class FlashcardCreateCommand extends Command
{
    protected $signature = 'flashcard:create';

    protected $description = 'Create a new flashcard';

    public function handle()
    {
        $this->output->write(sprintf("\033\143"));

        $question = $this->ask('What is the question?');

        $answer = $this->ask('What is the right answer to the question?');

        Flashcard::query()
            ->create([
                'question' => $question,
                'right_answer' => $answer
            ]);

        $this->newLine();
        $this->line('<fg=green>Flashcard has been created.</>');
        $this->newLine();

        $this->anticipate('Press return to see the main menu', ['Enter'], 'Enter');

        return 0;
    }
}
