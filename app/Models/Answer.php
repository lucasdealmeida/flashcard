<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property-read int $id
 * @property string $answer
 */
class Answer extends Model
{
    use HasFactory;

    protected $fillable = ['answer'];
}
