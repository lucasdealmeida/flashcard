<?php

namespace App\Models;

use App\Support\FlashcardCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property string $question
 * @property string $right_answer
 * @property Answer $answer
 * @property-read string $status
 * @property-read boolean $answer_is_right
 */
class Flashcard extends Model
{
    use HasFactory;

    protected $fillable = ['question', 'right_answer'];

    public function newCollection(array $models = []): FlashcardCollection
    {
        return new FlashcardCollection($models);
    }

    public function answer(): HasOne
    {
        return $this->hasOne(Answer::class);
    }

    public function getAnswerIsRightAttribute(): bool
    {
        return $this->answer?->answer === $this->right_answer;
    }

    public function getStatusAttribute(): string
    {
        if (!$this->answer) {
            return 'Not answered';
        }

        if ($this->answer_is_right) {
            return 'Correct';
        }

        return 'Incorrect';
    }
}
