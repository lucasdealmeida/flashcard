<?php

namespace App\Support;

use App\Models\Flashcard;
use Illuminate\Database\Eloquent\Collection;

class FlashcardCollection extends Collection
{
    public function onlyCorrect(): self
    {
        return $this->filter(fn(Flashcard $flashcard) => $flashcard->answer_is_right);
    }

    public function exceptCorrect(): self
    {
        return $this->filter(fn(Flashcard $flashcard) => $flashcard->status !== 'Correct');
    }

    public function onlyAnswered(): self
    {
        return $this->filter(fn(Flashcard $flashcard) => $flashcard->status !== 'Not answered');
    }

    public function percentageOf(float|int $total): int
    {
        if ($total <= 0) {
            return 0;
        }

        return (int) floor((100 * $this->count()) / $total);
    }
}
