<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\BufferedOutput;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function expectedTableWithFooterAsOutput($header, $rows, $footer): void
    {
        $table = (new Table($output = new BufferedOutput))
            ->setHeaders($header)
            ->setRows($rows)
            ->setFooterTitle($footer);

        $table->render();

        $lines = array_filter(
            explode(PHP_EOL, $output->fetch())
        );

        foreach ($lines as $line) {
            $this->expectedOutput[] = $line;
        }
    }
}
