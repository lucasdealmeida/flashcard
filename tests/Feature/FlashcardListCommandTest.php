<?php

namespace Tests\Feature;

use App\Models\Flashcard;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardListCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_all_flashcards()
    {
        $rows = Flashcard::factory()
            ->count(10)
            ->create()
            ->map
            ->only(['question', 'right_answer'])
            ->toArray();

        $this->artisan('flashcard:list')
            ->expectsTable(['Question', 'Answer'], $rows)
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->assertExitCode(0);
    }
}
