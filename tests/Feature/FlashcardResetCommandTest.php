<?php

namespace Tests\Feature;

use App\Models\Answer;
use App\Models\Flashcard;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardResetCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_reset()
    {
        /** @var Flashcard $flashcard1 */
        $flashcard1 = Flashcard::factory()->create(['right_answer' => 'foo']);
        /** @var Answer $answer1 */
        $answer1 = $flashcard1->answer()->create(['answer' => 'foo']);

        /** @var Flashcard $flashcard2 */
        $flashcard2 = Flashcard::factory()->create(['right_answer' => 'bar']);
        /** @var Answer $answer2 */
        $answer2 = $flashcard2->answer()->create(['answer' => 'foo']);

        $this->artisan('flashcard:reset')
            ->expectsQuestion('Do you wish to continue?', true)
            ->expectsOutput('The answers has been removed.')
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->assertExitCode(0);

        $this->assertDatabaseMissing('answers', ['id' => $answer1->id]);
        $this->assertDatabaseMissing('answers', ['id' => $answer2->id]);
    }

    public function test_reset_not_wish()
    {
        /** @var Flashcard $flashcard1 */
        $flashcard1 = Flashcard::factory()->create(['right_answer' => 'foo']);
        /** @var Answer $answer1 */
        $answer1 = $flashcard1->answer()->create(['answer' => 'foo']);

        /** @var Flashcard $flashcard2 */
        $flashcard2 = Flashcard::factory()->create(['right_answer' => 'bar']);
        /** @var Answer $answer2 */
        $answer2 = $flashcard2->answer()->create(['answer' => 'foo']);

        $this->artisan('flashcard:reset')
            ->expectsQuestion('Do you wish to continue?', false)
            ->assertExitCode(0);

        $this->assertDatabaseHas('answers', ['id' => $answer1->id]);
        $this->assertDatabaseHas('answers', ['id' => $answer2->id]);
    }
}
