<?php

namespace Tests\Feature;

use App\Models\Flashcard;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardPracticeCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_practice()
    {
        /** @var Flashcard $flashcard1 */
        $flashcard1 = Flashcard::factory()->create(['question' => 'Question 1', 'right_answer' => 'foo']);
        $flashcard1->answer()->create(['answer' => 'foo']);

        /** @var Flashcard $flashcard2 */
        $flashcard2 = Flashcard::factory()->create(['question' => 'Question 2', 'right_answer' => 'bar']);
        $flashcard2->answer()->create(['answer' => 'foo']);

        /** @var Flashcard $flashcard3 */
        $flashcard3 = Flashcard::factory()->create(['question' => 'Question 3', 'right_answer' => 'foobar']);

        $command = $this->artisan('flashcard:practice');

        $this->expectedTableWithFooterAsOutput(
            ['Question', 'Status'],
            [
                [$flashcard1->question, 'Correct'],
                [$flashcard2->question, 'Incorrect'],
                [$flashcard3->question, 'Not answered'],
            ],
            "33% of completion"
        );

        $command->expectsChoice(
                'Which question do you like to practice?',
                $flashcard3->question,
                [$flashcard2->question, $flashcard3->question, 'Back to main menu']
            )
            ->expectsQuestion("Write the answer for \"{$flashcard3->question}\"", 'foobar')
            ->expectsOutput('Answer is correct.');

        $this->expectedTableWithFooterAsOutput(
            ['Question', 'Status'],
            [
                [$flashcard1->question, 'Correct'],
                [$flashcard2->question, 'Incorrect'],
                [$flashcard3->question, 'Correct'],
            ],
            "66% of completion"
        );

        $command->expectsChoice(
                'Which question do you like to practice?','Back to main menu',
                [$flashcard2->question, 'Back to main menu']
            )
            ->assertExitCode(0);
    }
}
