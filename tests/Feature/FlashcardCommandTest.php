<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardCommandTest extends TestCase
{
    use RefreshDatabase;

    private CONST MENU_QUESTION = 'What would you like to do?';

    private CONST MENU_OPTIONS = [
        'Create a flashcard',
        'List all flashcards',
        'Practice',
        'Stats',
        'Reset',
        'Exit'
    ];

    public function test_menu()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }

    public function test_create_a_flashcard()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'Create a flashcard', self::MENU_OPTIONS)
            ->expectsQuestion('What is the question?', 'foo')
            ->expectsQuestion('What is the right answer to the question?', 'bar')
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }

    public function test_list_all_flashcards()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'List all flashcards', self::MENU_OPTIONS)
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }

    public function test_practice()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'Practice', self::MENU_OPTIONS)
            ->expectsChoice('Which question do you like to practice?', 'Back to main menu', ['Back to main menu'])
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }

    public function test_stats()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'Stats', self::MENU_OPTIONS)
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }

    public function test_reset()
    {
        $this->artisan('flashcard:interactive')
            ->expectsChoice(self::MENU_QUESTION,'Reset', self::MENU_OPTIONS)
            ->expectsQuestion('Do you wish to continue?', 'yes')
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->expectsChoice(self::MENU_QUESTION,'Exit', self::MENU_OPTIONS)
            ->assertExitCode(0);
    }
}
