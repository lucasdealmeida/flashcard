<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardCreateCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_a_new_flashcard()
    {
        $this->artisan('flashcard:create')
            ->expectsQuestion('What is the question?', 'foo')
            ->expectsQuestion('What is the right answer to the question?', 'bar')
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->assertExitCode(0);

        $this->assertDatabaseHas('flashcards', [
            'question' => 'foo',
            'right_answer' => 'bar'
        ]);
    }
}
