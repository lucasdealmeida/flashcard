<?php

namespace Tests\Feature;

use App\Models\Flashcard;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FlashcardStatsCommandTest extends TestCase
{
    use RefreshDatabase;

    public function test_stats()
    {
        /** @var Flashcard $flashcard1 */
        $flashcard1 = Flashcard::factory()->create(['right_answer' => 'foo']);
        $flashcard1->answer()->create(['answer' => 'foo']);

        /** @var Flashcard $flashcard2 */
        $flashcard2 = Flashcard::factory()->create(['right_answer' => 'bar']);
        $flashcard2->answer()->create(['answer' => 'foo']);

        Flashcard::factory()->create();

        $this->artisan('flashcard:stats')
            ->expectsOutput('3 questions in total.')
            ->expectsOutput('66% of questions that have an answer.')
            ->expectsOutput('33% of questions that have a correct answer.')
            ->expectsQuestion('Press return to see the main menu', 'Enter')
            ->assertExitCode(0);
    }
}
