### Start docker containers
```
docker-compose up -d
```

### Install PHP composer dependencies
```
docker run --rm --interactive --tty --volume $PWD:/app  composer install
```

### Config .env
```
docker exec flashcard_php cp .env.example .env && php artisan key:generate
```

### Run migrations
```
docker exec flashcard_php php artisan migrate
```

### Run tests
```
docker exec flashcard_php php artisan test
```
